package ru.scrum.dice;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class DiceApplicationTests {

	@Autowired private PersonRepository personRepository;
	@Autowired private MockMvc mockMvc;

	@Test
	void contextLoads() {

	}

	@Test
	public void shouldCreateEntity() throws Exception {
		String name = "Frodo";

		ResultActions results = mockMvc.perform(MockMvcRequestBuilders.post("/user")
						.contentType(MediaType.APPLICATION_JSON)
						.content("""
					{"name":"%s"}
					""".formatted(name)))
				.andExpect(MockMvcResultMatchers.status().isCreated());

		Person found = personRepository.findAll().get(0);
		assertEquals("Wrong person name", "Frodo", found.getName());
		results.andExpect(MockMvcResultMatchers.header().string("Location", Matchers.endsWith(found.getId().toString())));
	}

	@Test
	void getUserById() throws Exception {
		Person user = new Person();
		user.setName("Frodo");
		personRepository.save(user);
		Long personId = user.getId();

		mockMvc.perform(get("/user/{id}", personId)
				.contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(MockMvcResultMatchers.content().json(String.format("""
				{"id": %d, "name": "Frodo"}
			""", personId)));
	}

}
