package ru.scrum.dice;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.net.URI;
import java.util.List;

@RestController
public class UsersResource {

    private final PersonRepository personRepository;

    public UsersResource(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @PostMapping("/user")
    public ResponseEntity<Object> createUser(@RequestBody Person person) {
        Person savedPerson = personRepository.save(person);

        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(savedPerson.getId()).toUri();

        return ResponseEntity.created(location).build();

    }

    @GetMapping("/users")
    public ResponseEntity<List<Person>> getAllUsers() {
        return ResponseEntity.ok(personRepository.findAll());
    }

    @GetMapping("/user/{id}")
    public ResponseEntity<Object> getUser(@PathVariable Long id) {

        return ResponseEntity.ok(personRepository.findById(id));
    }
}
